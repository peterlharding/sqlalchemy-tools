#!/usr/bin/env python
#
#
#==========================================================================

import re
import sys

from sqlalchemy import sql, dialects
from sqlalchemy import *
from sqlalchemy.orm import create_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData

from sqlalchemy.dialects.mysql.base import \
    INTEGER, BIGINT, SMALLINT, TINYINT, VARCHAR, NVARCHAR, CHAR, \
    dialect

#--------------------------------------------------------------------------

import MySQL

#--------------------------------------------------------------------------

HDR1       = "#" + 79 * "="
HDR2       = "#" + 79 * "-"

#------------------------------------------------------------------------------

from CONFIG import DBUser, DBPassword

DBHost     = 'localhost'
DBName     = 'ft'

URI_MODEL  = 'mysql+mysqldb://%s:%s@%s/%s'

DB_URI     = URI_MODEL % (DBUser, DBPassword, DBHost, DBName)

#--------------------------------------------------------------------------
# Create and engine to access the datatbase and get the metadata

Base = declarative_base()

engine = create_engine(DB_URI, echo=False)

tables = engine.table_names()

print('')
print(("Generating models.py for the %s DB" % DBName))
print('')
print("Tables:")
print('')

#for table in tables:
#    print "  %s" % table

metadata = MetaData(bind=engine)

# Create a session to use the tables    

session = create_session(bind=engine)

#===============================================================================

class Enum(set):
   pass

   #--------------------------------------------------------------------

   def __getattr__(self, name):
      if name in self:
         return name
      raise AttributeError

#===============================================================================

DataTypes    = Enum(["String", "Integer", "Float", "DateTime", "Date", "Image"])

#===============================================================================

def get_reflected_tables():

    metadata = MetaData()

    metadata.reflect(bind=engine)

    tables = []

    for table in metadata.sorted_tables:

        print(table)

        tables.append(table)

    return tables

#-------------------------------------------------------------------------------

def gen_class_for_table(table):

    needs_blank = False

    lines = []

    lines.append("")
    lines.append(HDR1)
    lines.append("# %s" % table.name)
    lines.append(HDR2)
    lines.append('"""')
    # lines.append("  %s" % Column.HEADING)
    # lines.append("  %s" % Column.UNDERLINE)

    columns = []

    for column in table.columns:

        if column.primary_key:
            is_key = 'YES'
        else:
            is_key = ' '

        lines.append("  %-20s %-45s  %-5s  %s" % (column.name, column.type, column.nullable, is_key))

    lines.append("")
    lines.append('"""')
    lines.append(HDR2)

    lines.append("")
    lines.append("class %s(db.Model):" % table.name)
    lines.append("")
    lines.append("    _tablename_ = '%s'" % table.name)
    lines.append("")

    schema = []

    var_name = table.name.lower()[:-1]

    for column in table.columns:

        column_type = column.type # convert_type(column.type)

        schema.append(column.name)

        if column.primary_key:
            lines.append("    %-20s  = db.Column(%s, primary_key=True)" % (column.name, column_type))

        else:
            lines.append("    %-20s  = db.Column(%s)" % (column.name, column_type))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")
    lines.append('    def __str__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (table.name, schema[0], schema[0]))
    lines.append("")

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.append('    def __repr__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (table.name, schema[0], schema[0]))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.append("    @classmethod")
    lines.append("    def Save(cls, form):")
    lines.append("")
    lines.append("        now                     = datetime.now()")
    lines.append("")
    lines.append("        id                      = int(form['Id'])")
    lines.append("")
    lines.append("        Trace.Write('[%s::Save]  Id %%d' %% id)" % table.name)
    lines.append("")
    lines.append("        %-10s = %s.query.filter_by(Id=id).first()" % (var_name, table.name))
    lines.append("")

    for column in table.columns:

        data_type = None # Mysql.classify_type(column.type)

        assign_to = "%s.%s" % (var_name, column.name)

        if data_type == DataTypes.String:

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

        elif data_type == DataTypes.Integer:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = int(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        elif data_type == DataTypes.Float:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = float(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        else:  # What???

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

    lines.append("")           
    lines.append("        %s.ModifiedOn             = now" % var_name)
    lines.append("")

    lines.append("        db.session.flush()")
    lines.append("        db.session.commit()")

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")   

    lines.append("    @classmethod")
    lines.append("    def Add(cls, form):")
    lines.append("")    
    lines.append("        now                     = datetime.now()")
    lines.append("")    
    lines.append("        %-40s  = %s()" % (var_name, table.name))
    lines.append("")
    lines.append("        %-40s  = form['%s']" % (var_name, column.name))
    lines.append("")

    for column in table.columns:

        data_type = MySQL.classify_type(column.type)

        assign_to = "%s.%s" % (var_name, column.name)

        if data_type == DataTypes.String:

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

        elif data_type == DataTypes.Integer:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = int(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        elif data_type == DataTypes.Float:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = float(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        else:  # What???

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

    lines.append("")       
    lines.append("        %s.ModifiedOn         = now" % var_name)
    lines.append("") 
    lines.append("        db.session.add(%s)" % var_name)
    lines.append("        db.session.flush()")
    lines.append("        db.session.commit()")

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("") 

    return lines

#===============================================================================

PREAMBLE = """\
# encoding: utf-8

import sys

from datetime import datetime, timedelta
from hashlib  import md5

from dateutil import parser as datetime_parser
from dateutil.tz import tzutc

from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, current_app

from sqlalchemy.dialects.mysql import \\
    CHAR, \\
    INTEGER, \\
    BIGINT, \\
    SMALLINT, \\
    TINYINT, \\
    IMAGE

from . import db

from .exceptions import ValidationError

from .utils import split_url


ROLE_USER = 0
ROLE_ADMIN = 1

"""
#-------------------------------------------------------------------------------

def table_name_order(one, two):

    if one.name == two.name:
        return 0
    elif one.name > two.name:
        return 1
    else:
        return -1

#-------------------------------------------------------------------------------

def write_models():

    f_out = open("models_%s.py" % DBName, 'w+')

    f_out.write(PREAMBLE)

    m = MetaData()

    m.reflect(engine)

    tables = list(m.tables.values())

    tables.sort(table_name_order)

    for table in tables:

        print(("  %s" % table.name))

        model_class = gen_class_for_table(table)

        f_out.write('\n'.join(model_class))

    f_out.write("\n")

    f_out.close()

#-------------------------------------------------------------------------------

def create_mysql_model(table):

    for column in table.columns:
        is_pk       = column.primary_key
        is_nullable = column.nullable
        col_type    = column.type # SqlServer.convert_type(column.type)

        print(("  %20s: %-20s  %-30s" % (table.name, column.name, col_type)))

        # print("Primary keys in %s" % table.name)

        # for column in table.primary_key:
        #     print("  %s: %s" % (table.name, column.name))

#-------------------------------------------------------------------------------

def get_info():

    print("[get_info]  Table Info")

    m = MetaData()

    m.reflect(engine)

    for table in list(m.tables.values()):
        print(("Details for %s" % table.name))

        model = create_mysql_model(table)

#===============================================================================

# get_info()
write_models()

#-------------------------------------------------------------------------------

