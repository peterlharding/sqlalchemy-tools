
from sqlalchemy import sql, dialects

from sqlalchemy.dialects.mysql import \
        BIGINT, BINARY, BIT, BLOB, BOOLEAN, CHAR, DATE, \
        DATETIME, DECIMAL, DECIMAL, DOUBLE, ENUM, FLOAT, INTEGER, \
        LONGBLOB, LONGTEXT, MEDIUMBLOB, MEDIUMINT, MEDIUMTEXT, NCHAR, \
        NUMERIC, NVARCHAR, REAL, SET, SMALLINT, TEXT, TIME, TIMESTAMP, \
        TINYBLOB, TINYINT, TINYTEXT, VARBINARY, VARCHAR, YEAR

from .enum import Enum

#==============================================================================

DataTypes    = Enum([
                       "BIT",
                       "BLOB",
                       "BIGINT",
                       "Date",
                       "DateTime",
                       "Float",
                       "Integer",
                       "String",
                       "Text",
                       "TINYINT",
                       "XML"
                    ])

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

def classify_type(type_obj):

    #-----------------------------------------------------
    # MySQL specific stuff...
    #-----------------------------------------------------

    if type(type_obj) == dialects.mysql.types.VARCHAR:
        return DataTypes.String

    if type(type_obj) == dialects.mysql.types.INTEGER:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.types.LONGTEXT:
        return DataTypes.Text

    if type(type_obj) == dialects.mysql.types.VARCHAR:
        return DataTypes.String

    if type(type_obj) == dialects.mysql.types.VARCHAR:
        return DataTypes.String


    #-----------------------------------------------------

    if type(type_obj) == sql.sqltypes.INTEGER:
        return DataTypes.Integer

    if type(type_obj) == sql.sqltypes.CHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.NCHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.VARCHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.DATETIME:
        return DataTypes.DateTime

    if type(type_obj) == sql.sqltypes.DATE:
        return DataTypes.Date

    if type(type_obj) == sql.sqltypes.TEXT:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.NUMERIC:
        return DataTypes.Float

    if type(type_obj) == dialects.mysql.base.TINYINT:
        return DataTypes.Integer

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.base.BIT:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.base.VARBINARY:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.base.FLOAT:
        return DataTypes.Float

    return DataTypes.String

#-------------------------------------------------------------------------------

def convert_type(type_obj):

    print "[convert_type]  [%s]" % type_obj
    print "[convert_type]  [%s]" % type_obj.__dict__
    print "[convert_type]  Type [%s]" % type(type_obj)

    if type(type_obj) == dialects.mysql.types.VARCHAR:
        size = type_obj.length
        return 'String(%d)' % size

    if type(type_obj) == dialects.mysql.types.INTEGER:
        size = type_obj.display_width
        return 'Integer(%d)' % size

    if type(type_obj) == dialects.mysql.types.DECIMAL:
        precision = type_obj.precision
        scale     = type_obj.scale
        return 'Numeric(%s,%s)' % (precision, scale)

    if type(type_obj) == dialects.mysql.types.LONGTEXT:
        return 'Text'

    if type(type_obj) == sql.sqltypes.INTEGER:
        return 'Integer'

    if type(type_obj) == sql.sqltypes.CHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.NCHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.VARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'String(%s)' % len

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'String(%s)' % len

    if type(type_obj) == sql.sqltypes.DATETIME:
        return 'DateTime'

    if type(type_obj) == sql.sqltypes.DATE:
        return 'Date'

    if type(type_obj) == sql.sqltypes.TEXT:
        return 'Text'

    if type(type_obj) == sql.sqltypes.DECIMAL:
        precision = type_obj.precision
        return 'Numeric(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.NUMERIC:
        precision = type_obj.precision
        return 'Numeric(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return 'SMALLINT'

    if type(type_obj) == dialects.mysql.base.TINYINT:
        return 'TINYINT'

    if type(type_obj) == dialects.mysql.base.FLOAT:
        return 'Float'

    if type(type_obj) == dialects.mysql.base.BIT:
        return 'Integer'

    if type(type_obj) == dialects.mysql.base.VARBINARY:
        return 'VARBINARY'

    return 'UNKNOWN'


#-------------------------------------------------------------------------------

def to_sqla_type(type_obj):

    # print "[%s]" % type_obj
    # print "Type [%s]" % type(type_obj)

    if type(type_obj) == sql.sqltypes.BIGINT:
        return 'BIGINT'

    if type(type_obj) == dialects.mysql.BIT:
        return 'BIT'

    if type(type_obj) == sql.sqltypes.INTEGER:
        return 'Integer'

    if type(type_obj) == sql.sqltypes.CHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.NCHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.VARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'String(%s)' % len

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'String(%s)' % len

    if type(type_obj) == sql.sqltypes.DATETIME:
        return 'DateTime'

    if type(type_obj) == sql.sqltypes.DATE:
        return 'Date'

    if type(type_obj) == sql.sqltypes.TEXT:
        return 'Text'

    if type(type_obj) == sql.sqltypes.NUMERIC:
        precision = type_obj.precision
        return 'Numeric(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return 'SMALLINT'

    if type(type_obj) == dialects.mysql.TINYINT:
        return 'TINYINT'

    if type(type_obj) == dialects.mysql.VARBINARY:
        return 'VARBINARY'

    return 'Unknown'

#-------------------------------------------------------------------------------

def to_db_type(type_obj):

    # print "[%s]" % type_obj
    # print "Type [%s]" % type(type_obj)

    if type(type_obj) == sql.sqltypes.BIGINT:
        return 'BIGINT'

    if type(type_obj) == dialects.mysql.BIT:
        return 'BIT'

    if type(type_obj) == sql.sqltypes.INTEGER:
        return 'db.Integer'

    if type(type_obj) == sql.sqltypes.CHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.NCHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.VARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'db.String(%s)' % len

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'db.String(%s)' % len

    if type(type_obj) == sql.sqltypes.DATETIME:
        return 'db.DateTime'

    if type(type_obj) == sql.sqltypes.DATE:
        return 'db.Date'

    if type(type_obj) == sql.sqltypes.TEXT:
        return 'db.Text'

    if type(type_obj) == sql.sqltypes.NUMERIC:
        precision = type_obj.precision
        return 'db.Numeric(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return 'SMALLINT'

    if type(type_obj) == dialects.mysql.TINYINT:
        return 'TINYINT'

    if type(type_obj) == dialects.mysql.VARBINARY:
        return 'VARBINARY'

    return 'db.Unknown'

#-------------------------------------------------------------------------------

def csharp_type(type_obj):

    # print "[%s]" % type_obj
    # print "Type [%s]" % type(type_obj)

    if type(type_obj) == sql.sqltypes.INTEGER:
        return 'int'

    if type(type_obj) == sql.sqltypes.CHAR:
        len = type_obj.length
        return 'char'

    if type(type_obj) == sql.sqltypes.NCHAR:
        return 'string'

    if type(type_obj) == sql.sqltypes.VARCHAR:
        return 'string'

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        return 'string'

    if type(type_obj) == sql.sqltypes.DATETIME:
        return 'DateTime'

    if type(type_obj) == sql.sqltypes.DATE:
        return 'DateTime'

    if type(type_obj) == sql.sqltypes.TEXT:
        return 'string'

    if type(type_obj) == sql.sqltypes.NUMERIC:
        precision = type_obj.precision
        return 'Decimal(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return 'int'

    if type(type_obj) == dialects.mysql.TINYINT:
        return 'byte'

    if type(type_obj) == dialects.mysql.FLOAT:
        return 'float'

    if type(type_obj) == dialects.mysql.BIT:
        return 'int'

    if type(type_obj) == dialects.mysql.VARBINARY:
        return 'void'

    return 'Unknown'

#-------------------------------------------------------------------------------

def csharp_getfn(type_obj):

    # print "[%s]" % type_obj
    # print "Type [%s]" % type(type_obj)

    if type(type_obj) == sql.sqltypes.INTEGER:
        return 'Int32'

    if type(type_obj) == sql.sqltypes.CHAR:
        len = type_obj.length
        return 'Char'

    if type(type_obj) == sql.sqltypes.NCHAR:
        return 'String'

    if type(type_obj) == sql.sqltypes.VARCHAR:
        return 'String'

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        return 'String'

    if type(type_obj) == sql.sqltypes.DATETIME:
        return 'DateTime'

    if type(type_obj) == sql.sqltypes.DATE:
        return 'DateTime'

    if type(type_obj) == sql.sqltypes.TEXT:
        return 'String'

    if type(type_obj) == sql.sqltypes.NUMERIC:
        precision = type_obj.precision
        return 'Decimal(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return 'Int32'

    if type(type_obj) == dialects.mysql.TINYINT:
        return 'Byte'

    if type(type_obj) == dialects.mysql.FLOAT:
        return 'Float'

    if type(type_obj) == dialects.mysql.BIT:
        return 'Int32'

    if type(type_obj) == dialects.mysql.VARBINARY:
        return 'Void'

    return 'Unknown'

#-------------------------------------------------------------------------------

