# -*- coding: utf-8 -*-

#==============================================================================

INIT_PREAMBLE = """\
# encoding: utf-8

import sys

from datetime import datetime, timedelta
from hashlib  import md5

from dateutil import parser as datetime_parser
from dateutil.tz import tzutc

from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, current_app

from sqlalchemy.dialects.mysql import \\
    CHAR, \\
    INTEGER, \\
    BIGINT, \\
    SMALLINT, \\
    TINYINT, \\
    IMAGE

from . import db

from .exceptions import ValidationError

from .utils import split_url

ROLE_USER  = 0
ROLE_ADMIN = 1

"""

#==============================================================================

#--------------------------------------------------------------------------

CLASS_PREAMBLE = """\

from sqlalchemy import \\
    Column, \\
    Date, \\
    DateTime, \\
    Integer, \\
    String, \\
    Text

from sqlalchemy.dialects.mysql import \\
    BIGINT, \\
    BINARY, \\
    BIT, \\
    CHAR, \\
    DATE, \\
    DATETIME, \\
    DECIMAL, \\
    FLOAT, \\
    INTEGER, \\
    MONEY, \\
    NCHAR, \\
    NUMERIC, \\
    NVARCHAR, \\
    REAL, \\
    SMALLINT, \\
    TEXT, \\
    TIME, \\
    TIMESTAMP, \\
    TINYINT, \\
    VARBINARY, \\
    VARCHAR

from . import Base

"""

#==============================================================================

PREAMBLE = """\
# encoding: utf-8

import sys

from datetime import datetime, timedelta
from hashlib  import md5

from dateutil import parser as datetime_parser
from dateutil.tz import tzutc

from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, current_app

from sqlalchemy.dialects.mysql import \\
    CHAR, \\
    INTEGER, \\
    BIGINT, \\
    SMALLINT, \\
    TINYINT, \\
    IMAGE

from . import db

from .exceptions import ValidationError

from .utils import split_url


ROLE_USER = 0
ROLE_ADMIN = 1

"""

#==============================================================================


