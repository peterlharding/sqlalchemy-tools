#===============================================================================
# Methods used for Python 3 sorting
#-------------------------------------------------------------------------------

def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K:
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K

#-------------------------------------------------------------------------------

def table_name_order(one, two):

    if one.name == two.name:
        return 0
    elif one.name > two.name:
        return 1
    else:
        return -1

#===============================================================================
