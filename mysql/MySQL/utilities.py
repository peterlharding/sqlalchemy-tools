
from sqlalchemy import sql, dialects

#-------------------------------------------------------------------------------

HDR1       = "#" + 79 * "="
HDR2       = "#" + 79 * "-"

PREAMBLE = """\
# encoding: utf-8

import sys

from datetime import datetime, timedelta
from hashlib  import md5

from dateutil import parser as datetime_parser
from dateutil.tz import tzutc

from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, current_app

"""

MYSQL_IMPORT = """\


from sqlalchemy.dialects.mysql import CHAR, \\
                                      INTEGER, \\
                                      BIGINT, \\
                                      SMALLINT, \\
                                      TINYINT
"""

LOCAL_PREAMBLE = """\

from . import db

from .exceptions import ValidationError

from .utils import split_url


ROLE_USER = 0
ROLE_ADMIN = 1

"""

#===============================================================================

class Enum(set):
   pass

   #--------------------------------------------------------------------

   def __getattr__(self, name):
      if name in self:
         return name
      raise AttributeError

   #--------------------------------------------------------------------

#===============================================================================

DataTypes    = Enum([
                      "String",
                      "Integer",
                      "Float",
                      "DateTime",
                      "Date",
                      "Text",
                      "Image"
                    ])

#-------------------------------------------------------------------------------

def classify_type(type_obj):

    if type(type_obj) == sql.sqltypes.BIGINT:
        return DataTypes.Integer

    if type(type_obj) == sql.sqltypes.INTEGER:
        return DataTypes.Integer

    if type(type_obj) == sql.sqltypes.CHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.NCHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.VARCHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.DATETIME:
        return DataTypes.DateTime

    if type(type_obj) == sql.sqltypes.DATE:
        return DataTypes.Date

    if type(type_obj) == sql.sqltypes.TEXT:
        return DataTypes.String

    if type(type_obj) == sql.sqltypes.NUMERIC:
        return DataTypes.Float

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.base.BIGINT:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.base.BIT:
        return DataTypes.Integer

    if type(type_obj) == dialects.mysql.base.VARBINARY:
        return DataTypes.Integer


    return DataTypes.String

#-------------------------------------------------------------------------------

def convert_type(type_obj):

    # print "[%s]" % type_obj
    # print "Type [%s]" % type(type_obj)

    if type(type_obj) == sql.sqltypes.BIGINT:
        return 'db.Integer'

    if type(type_obj) == sql.sqltypes.INTEGER:
        return 'db.Integer'

    if type(type_obj) == sql.sqltypes.CHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.NCHAR:
        len = type_obj.length
        return 'CHAR(length=%s)' % len

    if type(type_obj) == sql.sqltypes.VARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'db.String(%s)' % len

    if type(type_obj) == sql.sqltypes.NVARCHAR:
        len = type_obj.length
        if len == None:  len =""
        return 'db.String(%s)' % len

    if type(type_obj) == sql.sqltypes.DATETIME:
        return 'db.DateTime'

    if type(type_obj) == sql.sqltypes.DATE:
        return 'db.Date'

    if type(type_obj) == sql.sqltypes.TEXT:
        return 'db.Text'

    if type(type_obj) == sql.sqltypes.NUMERIC:
        precision = type_obj.precision
        return 'db.Numeric(%s,0)' % precision

    if type(type_obj) == sql.sqltypes.SMALLINT:
        return 'SMALLINT'

    if type(type_obj) == dialects.mysql.base.BIT:
        return 'db.Integer'

    if type(type_obj) == dialects.mysql.base.VARBINARY:
        return 'VARBINARY'


#-------------------------------------------------------------------------------

def gen_class_for_table(table):

    needs_blank = False

    lines = []

    lines.append("")
    lines.append(HDR1)
    lines.append("# %s" % table.name)
    lines.append(HDR2)
    lines.append('"""')
    lines.append("  %s" % Column.HEADING)
    lines.append("  %s" % Column.UNDERLINE)

    columns = []

    for column in table.columns:

        if column.primary_key:
            is_key = 'YES'
        else:
            is_key = ' '

        try:
            lines.append("  %-20s %-45s  %-5s  %s" % (column.name, column.type, column.nullable, is_key))
        except:
            lines.append(">>>>>  %-20s" % column.name)

    lines.append("")
    lines.append('"""')
    lines.append(HDR2)

    lines.append("")
    lines.append("class %s(db.Model):" % table.name)
    lines.append("")
    lines.append("    __tablename__         = '%s'" % table.name)
    lines.append("")

    schema = []

    lc_name  = table.name.lower()

    if lc_name[-1:] == 's':
        var_name = lc_name[:-1]
    else:
        var_name = lc_name

    for column in table.columns:

        column_type = convert_type(column.type)

        schema.append(column.name)

        if column.primary_key:
            lines.append("    %-20s  = db.Column(%s, primary_key=True)" % (column.name, column_type))

        else:
            lines.append("    %-20s  = db.Column(%s)" % (column.name, column_type))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")
    lines.append('    def __str__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (table.name, schema[0], schema[0]))
    lines.append("")

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.append('    def __repr__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (table.name, schema[0], schema[0]))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.append("    @classmethod")
    lines.append("    def Save(cls, form):")
    lines.append("")
    lines.append("        now                     = datetime.now()")
    lines.append("")
    lines.append("        id                      = int(form['Id'])")
    lines.append("")
    lines.append("        Trace.Write('[%s::Save]  Id %%d' %% id)" % table.name)
    lines.append("")
    lines.append("        %-10s = %s.query.filter_by(Id=id).first()" % (var_name, table.name))
    lines.append("")

    for column in table.columns:

        data_type = classify_type(column.type)

        assign_to = "%s.%s" % (var_name, column.name)

        if data_type == DataTypes.String:

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

        elif data_type == DataTypes.Integer:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = int(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        elif data_type == DataTypes.Float:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = float(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        else:  # What???

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

    lines.append("")           
    lines.append("        %s.ModifiedOn             = now" % var_name)
    lines.append("")

    lines.append("        db.session.flush()")
    lines.append("        db.session.commit()")

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")   

    lines.append("    @classmethod")
    lines.append("    def Add(cls, form):")
    lines.append("")    
    lines.append("        now                     = datetime.now()")
    lines.append("")    
    lines.append("        %-40s  = %s()" % (var_name, table.name))
    lines.append("")
    lines.append("        %-40s  = form['%s']" % (var_name, column.name))
    lines.append("")

    for column in table.columns:

        data_type = classify_type(column.type)

        assign_to = "%s.%s" % (var_name, column.name)

        if data_type == DataTypes.String:

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

        elif data_type == DataTypes.Integer:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = int(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        elif data_type == DataTypes.Float:

            if needs_blank:
                lines.append("")

            lines.append("        try:")
            lines.append("            %-37s = float(form['%s'])" % (assign_to, column.name))
            lines.append("        except:")
            lines.append("            %-37s = 0" % assign_to)
            lines.append("")

            needs_blank = False

        else:  # What???

            lines.append("        %-40s  = form['%s']" % (assign_to, column.name))

            needs_blank = True

    lines.append("")       
    lines.append("        %s.ModifiedOn         = now" % var_name)
    lines.append("") 
    lines.append("        db.session.add(%s)" % var_name)
    lines.append("        db.session.flush()")
    lines.append("        db.session.commit()")

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("") 

    return lines

#===============================================================================
