#===============================================================================

class Column():
    HEADING   = "Field                          Type                                                Null   Key"
    UNDERLINE = "=====                          ====                                                ====   ==="

    #---------------------------------------------------------------------------

    def __init__(self, columns):
        self.Field    = columns[0]
        self.Type     = columns[1]
        self.Null     = columns[2]
        self.Key      = columns[3]
        self.Default  = columns[4]
        self.Extra    = columns[5]

    #---------------------------------------------------------------------------

    def __str__(self):
        return "%-20s %-20s %6s  %5s  %4s %s" % (
                self.Field,
                self.Type,
                self.Null,
                self.Key,
                self.Default,
                self.Extra
        )

        #---------------------------------------------------------------------------

#===============================================================================
