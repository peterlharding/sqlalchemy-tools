
from sqlalchemy import inspect
from sqlalchemy import MetaData

from .datatypes import convert_type

#===============================================================================

def create_mssql_model(table):

    for column in table.columns:
        is_pk       = column.primary_key
        is_nullable = column.nullable
        col_type    = convert_type(column.type)

        print("  %20s: %-20s  %-30s  %s" % (table.name, column.name, col_type, column.type))

        # print("Primary keys in %s" % table.name)

        # for column in table.primary_key:
        #     print("  %s: %s" % (table.name, column.name))

#-------------------------------------------------------------------------------
# Using inspector
#-------------------------------------------------------------------------------

def chk_tables(engine):

    inspector = inspect(engine)

    for table_name in inspector.get_table_names():

        print(table_name)

#-------------------------------------------------------------------------------

def inspect_tables(engine):

    inspector = inspect(engine)

    for table_name in inspector.get_table_names():
        for column in inspector.get_columns(table_name):
           print(("Column: %s" % column['name']))

#-------------------------------------------------------------------------------
# Using MetaData reflection
#-------------------------------------------------------------------------------

def list_tables(engine):

    m = MetaData()

    m.reflect(engine)

    for table in list(m.tables.values()):
        print(("%s" % table.name))

#-------------------------------------------------------------------------------

def print_tables(engine):

    tables   = engine.table_names()

    print()
    print("Generating models.py for the %s DB" % DBName)
    print()
    print("Tables:")
    print()

    for table in tables:

        print("  %s" % table)

#-------------------------------------------------------------------------------

def get_table_info(engine):

    print("[get_info]  Table Info")

    m = MetaData()

    m.reflect(engine)

    for table in list(m.tables.values()):

        print("Details for %s" % table.name)

        model = create_mssql_model(table)

        print()

#-------------------------------------------------------------------------------

def get_reflected_tables(engine):

    metadata = MetaData()

    metadata.reflect(bind=engine)

    tables = list(metadata.tables.values())

    tables = sorted(tables, key=cmp_to_key(table_name_order))

    for table in tables:

        print(table)

        tables.append(table)

    return tables

#-------------------------------------------------------------------------------

def summarize_model(engine):

    m = MetaData()

    m.reflect(engine)

    for table in list(m.tables.values()):

        print()
        print("%s" % table.name)
        print()

        for column in table.columns:

            is_pk       = column.primary_key
            is_nullable = column.nullable
            col_type    = to_db_type(column.type)

            print("  %20s: %-20s  %-30s" % (table.name, column.name, col_type))

        print()

#-------------------------------------------------------------------------------

