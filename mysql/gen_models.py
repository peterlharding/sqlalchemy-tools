#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#==============================================================================

import os
import re
import sys
import time
import getopt
import random
import pickle
import pprint
import logging
import urllib

import traceback

from datetime import datetime, timedelta

from sqlalchemy import create_engine, sql, dialects, inspect

from sqlalchemy.orm import create_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData

from sqlalchemy.dialects.mysql import \
    BIGINT, \
    BINARY, \
    BIT, \
    CHAR, \
    DATE, \
    DATETIME, \
    DECIMAL, \
    FLOAT, \
    INTEGER, \
    NCHAR, \
    NUMERIC, \
    NVARCHAR, \
    REAL, \
    SMALLINT, \
    TEXT, \
    TIME, \
    TIMESTAMP, \
    TINYINT, \
    VARBINARY, \
    VARCHAR

#------------------------------------------------------------------------------

import MySQL

from MySQL import \
    DataTypes, \
    Enum, \
    cmp_to_key, \
    table_name_order, \
    db_info, \
    CLASS_PREAMBLE, \
    INIT_PREAMBLE, \
    PREAMBLE

#--------------------------------------------------------------------------

__at_id__     = "@(#)  gen_models.py  [2.3.01]  2019-03-18"
__version__   = re.sub(r'.*\[([0-9.]*)\].*', r'\1', __at_id__)

quiet_flg     = False
verbose_flg   = False

debug_level   = 0

home_dir      = None

p_crlf        = re.compile(r'[\r\n]*')

pp            = pprint.PrettyPrinter(indent=3)

#------------------------------------------------------------------------------

HDR1       = "#" + 79 * "="
HDR2       = "#" + 79 * "-"

#------------------------------------------------------------------------------

from CONFIG import DBUser, DBPassword

DBHost     = 'localhost'
DBName     = 'somedb'

URI_MODEL  = 'mysql+mysqldb://%s:%s@%s/%s'

DB_URI     = URI_MODEL % (DBUser, DBPassword, DBHost, DBName)

#==============================================================================
# Create and engine to access the datatbase and get the metadata
#------------------------------------------------------------------------------

Base       = declarative_base()

engine     = create_engine(DB_URI, echo=False)

metadata   = MetaData(bind=engine)

session    = create_session(bind=engine)

#==============================================================================

class Column():
    HEADING   = "Field                Type                     Null  Key  Def  Extra"
    UNDERLINE = "=====                ====                     ====  ===  ===  ====="

    #------------------------------------------------------------------------

    def __init__(self, columns):
        self.Field    = columns[0]
        self.Type     = columns[1]
        self.Null     = columns[2]
        self.Key      = columns[3]
        self.Default  = columns[4]
        self.Extra    = columns[5]

    #------------------------------------------------------------------------

    def __str__(self):
        return "%-20s %-20s %6s  %5s  %4s %s" % (
                                                 self.Field,
                                                 self.Type,
                                                 self.Null,
                                                 self.Key,
                                                 self.Default,
                                                 self.Extra
                                              )

    #------------------------------------------------------------------------

#==============================================================================
"""
From MySQL:
+------------------+--------------+------+-----+---------+----------------+
| Field            | Type         | Null | Key | Default | Extra          |
+------------------+--------------+------+-----+---------+----------------+
| Staff_ID         | int(11)      | NO   | PRI | NULL    | auto_increment |
| Username         | varchar(12)  | NO   | UNI |         |                |
| StaffMnem        | varchar(5)   | YES  |     | NULL    |                |
| FirstName        | varchar(50)  | NO   |     |         |                |
| LastName         | varchar(50)  | NO   | MUL |         |                |
| Honorific        | varchar(20)  | YES  |     | NULL    |                |
| JobTitle         | varchar(50)  | YES  |     | NULL    |                |
| Address          | varchar(255) | YES  |     | NULL    |                |
| City             | varchar(50)  | YES  |     | NULL    |                |
| State            | varchar(20)  | YES  |     | NULL    |                |
| Postcode         | varchar(20)  | YES  | MUL | NULL    |                |
| Email            | varchar(60)  | YES  | UNI | NULL    |                |
| SMS              | varchar(30)  | YES  |     | NULL    |                |
| HomePhone        | varchar(30)  | YES  |     | NULL    |                |
| WorkPhone        | varchar(30)  | YES  |     | NULL    |                |
| MobilePhone      | varchar(30)  | YES  |     | NULL    |                |
| FaxPhone         | varchar(30)  | YES  |     | NULL    |                |
| PagerPhone       | varchar(30)  | YES  |     | NULL    |                |
| UID              | int(1)       | NO   |     | 0       |                |
| UnixUsername     | varchar(10)  | YES  |     | NULL    |                |
| IsAdmin          | int(1)       | NO   |     | 0       |                |
| UseSlider        | int(1)       | YES  |     | NULL    |                |
| WorkID           | varchar(12)  | YES  |     | NULL    |                |
| FromDate         | date         | YES  |     | NULL    |                |
| StartTime        | varchar(10)  | YES  |     | NULL    |                |
| ToDate           | date         | YES  |     | NULL    |                |
| EndTime          | varchar(10)  | YES  |     | NULL    |                |
| Client_FK        | int(10)      | YES  |     | NULL    |                |
| Project_FK       | int(10)      | YES  |     | NULL    |                |
| Workcode_FK      | int(10)      | YES  |     | NULL    |                |
| Defaults_FK      | int(10)      | YES  | MUL | NULL    |                |
| GLCode           | int(10)      | YES  |     | NULL    |                |
| HoursPerDay      | int(10)      | YES  |     | NULL    |                |
| LeaveFlag        | tinyint(1)   | YES  |     | NULL    |                |
| CommencementDate | date         | YES  |     | NULL    |                |
| TerminationDate  | date         | YES  |     | NULL    |                |
| Active           | int(10)      | YES  |     | NULL    |                |
| Flag             | int(10)      | YES  |     | NULL    |                |
| Notes            | longtext     | YES  |     | NULL    |                |
| LastLogin        | date         | YES  |     | NULL    |                |
| ModifiedBy       | varchar(10)  | NO   |     |         |                |
| DateModified     | date         | YES  |     | NULL    |                |
| ExpenseLimit     | decimal(9,2) | NO   |     | 0.00    |                |
| Manager_FK       | varchar(12)  | NO   | MUL |         |                |
| Email2           | varchar(60)  | YES  |     | NULL    |                |
| SitePhone        | varchar(30)  | YES  |     | NULL    |                |
+------------------+--------------+------+-----+---------+----------------+
46 rows in set (0.00 sec)
"""

def get_staff():

    staff_list = session.query(Staff).all()    

    for s in staff_list:
        print("%-10s %-5s  %-20s  %-20s  %s" % (
            s.Staff_ID,
            s.StaffMnem,
            s.LastName,
            s.FirstName,
            (s.IsAdmin == 1),
        ))

#------------------------------------------------------------------------------

def get_reflected_tables():

    meta = MetaData()

    meta.reflect(bind=engine)

    tables = []

    for table in meta.sorted_tables:
        print(table)

        tables.append(table)

    return tables

#------------------------------------------------------------------------------

def get_databases():
    q = engine.execute('SHOW DATABASES')

    databases = q.fetchall()

    print(databases)

    return dtabases

#------------------------------------------------------------------------------

def desc(tablename):

    q = engine.execute('DESC %s' % tablename)

    column_descriptions = q.fetchall()

    lines = []

    lines.append("")
    lines.append(HDR1)
    lines.append('"""')
    lines.append("  %s" % Column.HEADING)
    lines.append("  %s" % Column.UNDERLINE)

    columns = []

    for column_data in column_descriptions:
        column = Column(column_data)
        lines.append("  %s" % column)
        columns.append(column)

    lines.append("")
    lines.append('"""')
    lines.append(HDR2)

    return columns, lines

#------------------------------------------------------------------------------

p_bigint   = re.compile(r'bigint')
p_char     = re.compile(r'\bchar\b')
p_date     = re.compile(r'\bdate\b')
p_decimal  = re.compile(r'\bdecimal\b')
p_int      = re.compile(r'int')
p_smallint = re.compile(r'smallint')
p_tinyint  = re.compile(r'tinyint')
p_text     = re.compile(r'text')
p_time     = re.compile(r'timestamp')
p_varchar  = re.compile(r'varchar')

def convert_type(column_type):

    m = p_varchar.search(column_type)
    
    if m:
        return re.sub('varchar', 'db.String', column_type)

    m = p_char.search(column_type)

    if m:
        return re.sub('char\(', 'db.CHAR(length=', column_type)

    m = p_smallint.search(column_type)

    if m:
        return 'db.SMALLINT'

    m = p_bigint.search(column_type)

    if m:
        return 'db.BIGINT'

    m = p_tinyint.search(column_type)

    if m:
        return 'db.TINYINT'

    m = p_int.search(column_type)

    if m:
        return 'db.Integer'

    m = p_text.search(column_type)

    if m:
        return 'db.Text'

    m = p_date.search(column_type)

    if m:
        return 'db.Date'

    m = p_time.search(column_type)

    if m:
        return 'db.DateTime'

    m = p_decimal.search(column_type)

    if m:
        return re.sub('decimal', 'db.Numeric', column_type)

    return column_type

#------------------------------------------------------------------------------

def gen_class_for_table(tablename, schema):

    lines = []

    lines.append("")
    lines.append("class %s(db.Model):" % tablename)
    lines.append("")
    lines.append("    _tablename_ = '%s'" % tablename)
    lines.append("")

    for column in schema:

        ty = convert_type(column.Type)

        if column.Key == 'PRI':
            lines.append("    %-20s  = db.Column(%s, primary_key = True)" % (column.Field, ty))

        else:
            lines.append("    %-20s  = db.Column(%s)" % (column.Field, ty))

    lines.append("")
    lines.append('    #' + 66 * '-')
    lines.append("")
    lines.append('    def __repr__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (tablename, schema[0].Field, schema[0].Field))
    lines.append("")
    lines.append('    #' + 66 * '-')
    lines.append("")

    return lines

#------------------------------------------------------------------------------

def create_model(table_name):

    columns, description = desc(table_name)

    class_defn = gen_class_for_table(table_name, columns)

    description.extend(class_defn) 

    return '\n'.join(description)

#==============================================================================



f_out = open("%s_model.py" % DB, 'w+')

f_out.write(PREAMBLE)

for table in tables:
    model = create_model(table)
    f_out.write(model)

f_out.write("\n")

f_out.close()

#==============================================================================

"""

This was an early version of the model generation script.

"""

