#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#==============================================================================

import os
import re
import sys
import time
import getopt
import random
import pickle
import pprint
import logging
import urllib

import traceback

from datetime import datetime, timedelta

from sqlalchemy import create_engine, sql, dialects, inspect

from sqlalchemy.orm import create_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import MetaData

from sqlalchemy.dialects.mysql import \
    BIGINT, \
    BINARY, \
    BIT, \
    CHAR, \
    DATE, \
    DATETIME, \
    DECIMAL, \
    FLOAT, \
    INTEGER, \
    NCHAR, \
    NUMERIC, \
    NVARCHAR, \
    REAL, \
    SMALLINT, \
    TEXT, \
    TIME, \
    TIMESTAMP, \
    TINYINT, \
    VARBINARY, \
    VARCHAR

#------------------------------------------------------------------------------

import MySQL

from MySQL import \
    DataTypes, \
    Enum, \
    cmp_to_key, \
    table_name_order, \
    db_info, \
    CLASS_PREAMBLE, \
    INIT_PREAMBLE, \
    PREAMBLE

#--------------------------------------------------------------------------

__at_id__     = "@(#)  gen_sqla_models.py  [2.3.01]  2019-03-18"
__version__   = re.sub(r'.*\[([0-9.]*)\].*', r'\1', __at_id__)

quiet_flg     = False
verbose_flg   = False

debug_level   = 0

home_dir      = None

p_crlf        = re.compile(r'[\r\n]*')

pp            = pprint.PrettyPrinter(indent=3)

#------------------------------------------------------------------------------

HDR1       = "#" + 79 * "="
HDR2       = "#" + 79 * "-"

#------------------------------------------------------------------------------

from CONFIG import DBUser, DBPassword

DBHost     = 'localhost'
DBName     = 'somedb'

URI_MODEL  = 'mysql+mysqldb://%s:%s@%s/%s'

DB_URI     = URI_MODEL % (DBUser, DBPassword, DBHost, DBName)

#==============================================================================
# Create and engine to access the datatbase and get the metadata
#------------------------------------------------------------------------------

Base       = declarative_base()

engine     = create_engine(DB_URI, echo=False)

metadata   = MetaData(bind=engine)

session    = create_session(bind=engine)

#==============================================================================

def gen_class_heading_for_table(table):

    lines  = []

    lines.append("")
    lines.append(HDR1)
    lines.append("# %s" % table.name)
    lines.append(HDR2)
    lines.append('"""')
    lines.append("  %s" % MySQL.Column.HEADING)
    lines.append("  %s" % MySQL.Column.UNDERLINE)

    columns = []

    primary_key = 'UNDEFINED'

    for column in table.columns:

        # print("Column: %s  IsKey |%s|" % (column.name, column.primary_key))

        if column.primary_key:
            primary_key = column.name
            is_key = 'YES'
        else:
            is_key = ' '

        try:
            lines.append("  %-30s %-50s  %-5s  %s" % (column.name, column.type, column.nullable, is_key))
        except:
            lines.append(">>>>>  %-20s" % column.name)

    lines.append("")
    lines.append('"""')
    lines.append(HDR2)

    if primary_key == 'UNDEFINED':
        print("Table - %s - is missing a Primary Key!" % table.name)

    return lines, primary_key

#------------------------------------------------------------------------------

def gen_class_schema_for_table(table):

    lines  = []
    schema = []

    for column in table.columns:

        column_type = MySQL.to_sqla_type(column.type)

        schema.append(column.name)

        if column.primary_key:
            lines.append("    %-30s  = Column(%s, primary_key=True)" % (column.name, column_type))

        else:
            lines.append("    %-30s  = Column(%s)" % (column.name, column_type))

    return lines, schema

#------------------------------------------------------------------------------

def gen_str_function(table, private_key, schema):

    lines = []

    lines.append('    def __str__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (table.name, private_key, schema[1]))

    return lines

#------------------------------------------------------------------------------

def gen_repr_function(table, private_key, schema):

    lines = []

    lines.append('    def __repr__(self):')
    lines.append("        return '<%s: %%r <%%s>>' %% (self.%s, self.%s)" % (table.name, private_key, schema[1]))

    return lines

#------------------------------------------------------------------------------

def gen_jsonify_function(table):

    lines = []

    lines.append('    def jsonify(self):')
    lines.append("        return {")

    try:

        for column in table.columns:

            try:
                data_type = MySQL.classify_type(column.type)
            except Exception as e:
                print("[gen_jsonify_function]  classify_type(%s) -> Exception %s" % (column.type, e))
                data_type = "BAD"
                sys.exit(3)

            column_tag = '"%s"' % column.name

            if data_type == DataTypes.DateTime:
                lines.append("            %-30s : self.%s.strftime('%%Y-%%m-%%d %%H:%%M:%%S')," % (
                                                      column_tag, column.name))

            elif data_type == DataTypes.Date:
                lines.append("            %-30s : self.%s.strftime('%%Y-%%m-%%d')," % (
                                                      column_tag, column.name))

            else:
                lines.append("            %-30s : self.%s," % (
                                                      column_tag, column.name))
    except Exception as ex:
        print("[gen_jsonify_function] column %s" % column)
        print("[gen_jsonify_function] ex %s" % ex)
        sys.exit(2)

    lines.append("        }")

    return lines

#------------------------------------------------------------------------------

def gen_formdata_function(table, primary_key):

    lines = []

    needs_blank = False

    var_name = table.name.lower()

    lines = []

    lines.append("    @classmethod")
    lines.append("    def GetFormData(cls, %s, form):" % var_name)
    lines.append("")

    try:

        for column in table.columns:

            if column.name == primary_key:  continue

            try:
                data_type = MySQL.classify_type(column.type)
            except Exception as e:
                print("[gen_formdata_function]  classify_type(%s) -> Exception %s" % (column.type, e))
                data_type = "BAD"
                sys.exit(3)

            assign_to = "%s.%s" % (var_name, column.name)

            if data_type == DataTypes.String:

                lines.append("        %-40s = form['%s']" % (assign_to, column.name))

                needs_blank = True

            elif data_type == DataTypes.Integer:

                if needs_blank:
                    lines.append("")

                lines.append("        try:")
                lines.append("            %-36s = int(form['%s'])" % (assign_to, column.name))
                lines.append("        except:")
                lines.append("            %-36s = 0" % assign_to)
                lines.append("")

                needs_blank = False

            elif data_type == DataTypes.Float:

                if needs_blank:
                    lines.append("")

                lines.append("        try:")
                lines.append("            %-36s = float(form['%s'])" % (assign_to, column.name))
                lines.append("        except:")
                lines.append("            %-36s = 0" % assign_to)
                lines.append("")

                needs_blank = False

            else:  # What???

                lines.append("        %-40s = form['%s']" % (assign_to, column.name))

                needs_blank = True

    except Exception as ex:
        print("[gen_formdata_function] column %s" % column)
        print("[gen_formdata_function] ex %s" % ex)
        sys.exit(2)

    return lines

#-------------------------------------------------------------------------------

def gen_save_function(table, primary_key):

    needs_blank = False

    var_name = table.name.lower()

    lines = []

    lines.append("    @classmethod")
    lines.append("    def Save(cls, form):")
    lines.append("")
    lines.append("        now                                      = datetime.now()")
    lines.append("")
    lines.append("        id                                       = int(form['%s'])" % primary_key)
    lines.append("")
    lines.append("        Trace.Write('[%s::Save]  Id %%d' %% id)" % table.name)
    lines.append("")
    lines.append("        %s = %s.query.filter_by(%s=id).first()" % (var_name, table.name, primary_key))
    lines.append("")
    lines.append("        cls.GetFormData(%s, form)" % var_name)
    lines.append("")
    lines.append("        session.flush()")
    lines.append("        session.commit()")

    return lines

#-------------------------------------------------------------------------------

def gen_add_function(table):

    needs_blank = False

    var_name = table.name.lower()

    lines = []

    lines.append("    @classmethod")
    lines.append("    def Add(cls, form):")
    lines.append("")
    lines.append("        %s = %s()" % (var_name, table.name))
    lines.append("")
    lines.append("        cls.GetFormData(%s, form)" % var_name)
    lines.append("")
    lines.append("        session.add(%s)" % var_name)
    lines.append("")
    lines.append("        session.flush()")
    lines.append("        # session.commit()")

    return lines

#------------------------------------------------------------------------------

def gen_class_for_table(table):

    # print("[gen_class_for_table] Started table - %s" % table)

    needs_blank = False

    lines = []

    heading_lines, primary_key = gen_class_heading_for_table(table)

    lines.extend(heading_lines)

    lines.append("")
    lines.append("class %s(db.Model):" % table.name)
    lines.append("")
    lines.append("    __tablename__                   = '%s'" % table.name)
    lines.append("    __bindkey__                     = 'xyzzy'")
    lines.append("")

    schema_lines, schema = gen_class_schema_for_table(table)

    lines.extend(schema_lines)

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.extend(gen_str_function(table, primary_key, schema))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.extend(gen_repr_function(table, primary_key, schema))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.extend(gen_jsonify_function(table))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.extend(gen_formdata_function(table, primary_key))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.extend(gen_save_function(table, primary_key))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("")

    lines.extend(gen_add_function(table))

    lines.append("")
    lines.append('    #' + 75 * '-')
    lines.append("") 

    return lines

#==============================================================================

def write_models(remove=False):

    module_path = "%s.Sqla" % DBName

    try:
        os.mkdir(module_path)

    except:
        print("Module path - %s - exists already.  Remove and re-run" % module_path)
        sys.exit(1)

    f_out = open("%s/models.py" % module_path, 'w+')

    f_out.write(INIT_PREAMBLE)
    f_out.write("\n\n")

    f_out.close()

    m = MetaData()

    m.reflect(engine)

    tables = list(m.tables.values())

    tables = sorted(tables, key=cmp_to_key(table_name_order))

    for table in tables:

        f_out = open("%s/%s.py" % (module_path, table.name), 'w+')

        f_out.write(CLASS_PREAMBLE)

        print("  %s" % table.name)

        try:
            model_class = gen_class_for_table(table)

            # print(model_class)

            f_out.write('\n'.join(model_class))
            f_out.write("\n\n")

        except Exception as e:

            print("[write_models]")
            print("[write_models]  Exception on table - %s" % table.name)
            print("[write_models]  Exception - %s" % e)

            traceback.print_exc()

            sys.exit(2)

        f_out.close()

#==============================================================================

def usage():
    print(__doc__)

#------------------------------------------------------------------------------

def main(argv):
    global debug_level
    global quiet_flg
    global verbose_flg
    global target
    global home_dir

    try:
        home_dir = os.environ['HOME']
    except:
        print("Set HOME environment variable and re-run")
        sys.exit(0)

    Modes    = Enum([
                       "Info",
                       "Generate",
                       "ListTables",
                       "PrintTables",
                       "Reflect",
                    ])

    mode       = Modes.Info
    filename   = None
    remove_flg = False

    try:
        opts, args = getopt.getopt(argv, "dD:f:ghilpqrRvV?",
                ("debug", "debug-level=",
                 "file=",
                 "generate",
                 "info",
                 "list-tables",
                 "print-tables",
                 "remove",
                 "reflect",
                 "help", "quiet", "verbose", "version"))
    except getopt.error as msg:
        usage()
        return 1

    for opt, arg in opts:
        if opt in ("-?", "-h", "--help"):
            usage()
            return 0
        elif opt in ('-d', '--debug'):
            debug_level                        += 1
        elif opt in ('-D', '--debug-level'):
            debug_level                         = int(arg)
        elif opt in ('-g', '--generate'):
            mode                                = Modes.Generate
        elif opt in ('-f', '--file'):
            filename                            = arg
        elif opt in ('-i', '--info'):
            mode                                = Modes.Info
        elif opt in ('-l', '--list-tables'):
            mode                                = Modes.ListTables
        elif opt in ('-p', '--print'):
            mode                                = Modes.PrintTables
        elif opt in ('-q', '--quiet'):
            quiet_flg                           = True
        elif opt in ('-r', '--remove'):
            remove_flg                          = True
        elif opt in ('-R', '--reflect'):
            mode                                = Modes.Reflect
        elif opt in ('-v', '--verbose'):
            verbose_flg                         = True
        elif opt in ('-V', '--version'):
            if quiet_flg:
                print(__version__)
            else:
                print("[gen_sqla_models]  Version: %s" % __version__)
            return 1
        else:
            usage()
            return 1

    rest = []

    if args:
        for arg in args:
            rest.append(arg)

    sys.stderr.write("[gen_sqla_models]  Working directory is %s\n" % os.getcwd())

    if (debug_level > 0): sys.stderr.write("[gen_sqla_models]  Debugging level set to %d\n" % debug_level)

    sys.stderr.flush()

    if mode == Modes.Info:
        db_info.get_table_info(engine)
    elif mode == Modes.Generate:
        write_models(remove=remove_flg)
    elif mode == Modes.ListTables:
        db_info.list_tables(engine)
    elif mode == Modes.Reflect:
        db_info.get_reflected_tables(engine)
    else:
        pass

    return 0

#------------------------------------------------------------------------------

if __name__ == '__main__' or __name__ == sys.argv[0]:
    try:
        sys.exit(main(sys.argv[1:]))
    except KeyboardInterrupt as e:
        print("[gen_sqla_models]  Interrupted!")

#------------------------------------------------------------------------------

"""
Revision History:

     Date     Who   Description
   --------   ---   -----------------------------------------------------------
   20190318   plh   Converted scrip to use args

Problems to fix:

To Do:

Issues:


"""

